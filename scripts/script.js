window.addEventListener("load", function () {

document.addEventListener("keydown", keydownHandler);

function keydownHandler(evt) {
    let keyPressed;
    const btnsOnPage = document.querySelector(".btn-wrapper").children;
    if (evt.code.includes("Key")){
        keyPressed = evt.code[evt.code.length-1];
    }
    else {
        keyPressed = evt.code;
    }
    for (const btn of btnsOnPage) {
        if (btn.innerText !== keyPressed){
            btn.classList.remove("btn_selected");
        }
        else {
            btn.classList.add("btn_selected");
        }
    }
}

});
