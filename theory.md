Использовать события клавиатуры для обработки ввода данных в поле 
не целесообразно. Текст необязательно может быть именно напечатан вручную, пользователь
может вставить уже скопированный текст с помощью мышки, и тогда событие не сработает.
Кроме этого, некоторые мобильные устройства, могут не генерировать события клавиатуры, 
а просто вставлять текст в поле ввода. 
